# Changelog

### [0.0.5](https://gitlab.com/aelham/react-frontend/compare/v0.0.4...v0.0.5) (2022-06-13)

### [0.0.4](https://gitlab.com/aelham/react-frontend/compare/v0.0.3...v0.0.4) (2022-04-27)

### Features

- add paragraph ([f58dbd1](https://gitlab.com/aelham/react-frontend/commit/f58dbd174ec10285b8b985a72ae0ac947dfd15e9))

### Bug Fixes

- lint failure ([d4f0ca0](https://gitlab.com/aelham/react-frontend/commit/d4f0ca0f434a63d4105b6da59479db1e886e8749))

### Maintenance

- add tests ([5c058cb](https://gitlab.com/aelham/react-frontend/commit/5c058cb59f1cbb7f1f51b531365b21e0b23d546f))

### 0.0.3 (2022-03-16)

### Features

- add conventional commits, changelog generation, prettier, qol improvements ([09655bd](https://gitlab.com/aelham/react-frontend/commit/09655bd81cb1d11a3e90443f288df7b3d5ae6273))
- Initial commit from Create Next App ([90767d3](https://gitlab.com/aelham/react-frontend/commit/90767d381ad83dd435d973cc7d299a608bd5ad8f))

### Maintenance

- add husky hooks ([f3c31f2](https://gitlab.com/aelham/react-frontend/commit/f3c31f2d1925ae202cc0c4f91221c2b5984a2fde))
- **release:** 0.0.2 ([397122f](https://gitlab.com/aelham/react-frontend/commit/397122f01a0b2edf76e72b2f0be09b82ac579a23))

### 0.0.2 (2022-02-03)

### Features

- add conventional commits, changelog generation, prettier, qol improvements ([09655bd](https://gitlab.com/zchua-gtlb/next-frontend/commit/09655bd81cb1d11a3e90443f288df7b3d5ae6273))
- Initial commit from Create Next App ([90767d3](https://gitlab.com/zchua-gtlb/next-frontend/commit/90767d381ad83dd435d973cc7d299a608bd5ad8f))
