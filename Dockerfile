FROM node:16.14.0-alpine as base
LABEL maintainer="Zane J Chua zchua@gitlab.com"
RUN mkdir -p /srv/http/www/next-frontend
WORKDIR /srv/http/www/next-frontend
COPY package.json .
COPY yarn.lock .
RUN apk --no-cache --update add python3 make g++ ca-certificates openssl
RUN yarn install

FROM base as build
COPY .  /srv/http/www/next-frontend
